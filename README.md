Introduction
============

This code was produced to assess the impact of norovirus vacciantion at
population level. The MATLAB code was produced by KAMG and the R by
AJKC.

The relevant citation and documentation can be found at

K. A. M. Gaythorpe, C. L. Trotter and A. J. K. Conlan. "Modelling
norovirus transmission and vaccination" 2018 (In review)
