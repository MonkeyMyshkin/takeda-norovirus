function [ LL ] = NBLikelihood( StratifiedCases, ReportedInfectionNumber, Dispersion)
% %Calcualtes negative binomial likelihood for German Case notifications
% StratifiedCases=StratifiedCases(:,[end]);
% ReportedInfectionNumber=ReportedInfectionNumber(:,[end]);

%NB the model cases must be in integers
if max(ReportedInfectionNumber(:))<1
    LL=-inf;
else
    R=1/Dispersion;
    
    P=bsxfun(@rdivide,bsxfun(@times,Dispersion,ReportedInfectionNumber ),bsxfun(@plus,1,bsxfun(@times,Dispersion,ReportedInfectionNumber )) );
    
    LogLike=log( nbinpdf( round( StratifiedCases) , R, 1-P ));
    
    LL=sum(LogLike(:));
end

end


