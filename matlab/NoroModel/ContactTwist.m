function [ TwistedContactMatrix ] = ContactTwist( ContactAgesPerAgeGroup,T ,k, d )
%%
%Matrix to reduce or enhance mixing between subgroups off diagonal
%direction is as per Meyer and Held 2016
%ContacTwist takes raw contact matrix and emphasises or lessens the effect
%of within age group mixing. It also uses a mixture model for symmetric and
%assymetric contacts

%Inputs:
%ContactAgesPerAgeGroup=raw contact matrix output from MakeContactMatrix
%T=number of participants in each age group output from MakeContactMatrix
%k=Degree of diagonalisation of contact matrix, k=0 suggest C=I, k=1 suggests C is unchanged
%d=Degree of assymetry. If d=1, matrix is assymmetic, else d=0 suggests matrix is symmetric

%Output:
%TwistedContactMatrix= Partially symmetrised, partially diagonalised
%                      contact matrix
%%

noAgeGroups=length(T);

rowSums=sum(ContactAgesPerAgeGroup,2);
NORMcontactMatrix=bsxfun(@rdivide,ContactAgesPerAgeGroup,rowSums);

[V,D]=eig(NORMcontactMatrix);   %find eigenvalues and eigenvectors of contact matrix

NewContactMatrix= (V * D^k) / V ;  %Twist

%truncate at zero
NewContactMatrix(NewContactMatrix<0)=0;

%real
NewContactMatrix=real(NewContactMatrix);

%un normalize
NewContactMatrix=bsxfun(@times,NewContactMatrix,rowSums);



%reciprocity
AsymContactMatrix=zeros(noAgeGroups);
SymContactMatrix=zeros(noAgeGroups);
for index = 1 : noAgeGroups
    for jndex = 1 : noAgeGroups
        
        %AsymContactMatrix(index,jndex) =NewContactMatrix(index,jndex);
        SymContactMatrix(index,jndex) =( ContactAgesPerAgeGroup(index,jndex)* T(index) + ContactAgesPerAgeGroup(jndex,index)* T(jndex) )/(2*T(index));
    end
end

TwistedContactMatrix=  SymContactMatrix;
end





