function [ ] = PlotVaccinationScenarios( Scenario,Reductions, Reduction_by_age)
%takes vacciantion model output and plots reductions against
%every vaccination scenario component


% inf, symp, dur
x_values=linspace(min(Reductions(:))-0.5*(max(Reductions(:))-min(Reductions(:))),max(Reductions(:))+0.5*(max(Reductions(:))-min(Reductions(:))));

%Characteristics
%EffectiveCoverage=[0.5*0.9 0.7*0.5]/365;
% A0=[1;zeros(Lmax-1,1)];
% A1=[0;1;zeros(Lmax-2,1)];
% A2=[0;0;1;zeros(Lmax-3,1)];
% A3=[zeros(50,1); ones(20,1);zeros(Lmax-70,1)];
% A4=[zeros(70,1); ones(11,1)];
% A5=zeros(Lmax,1); A5([66,71,76,81])=1;
% A6=[zeros(50,1); ones(31,1)];  
% AgeGroupsTargeted=[A0, A1, A1 + A6, A1 + A5];
VaccineDuration=1/(5*365);
SymptomReduction=[0 0.3 0.5 0.7 1]; %this scales the proportion symptomatic
InfectiousnessReduction=[0 0.3 0.5 0.7 1];
SusceptibilityReduction=[0 0.3 0.5 0.7 1];
Sterilizing=[1 0];


Reductions(Reductions<0)=0; %ignore negatives- put check in earlier

%% Boxplots
% figure

%age group targeted
% subplot(2,3,1)
% try
%     vec=nan;    g=nan;
%     for agegrp=1:6
%         indices=find(Scenario(:,1)==agegrp);
%         vec=[vec;Reductions(indices,:)];
%         g=[g;agegrp*ones(length(indices),1)];
%     end
%     
%     H=boxplot(vec(2:end),g(2:end),'Boxstyle','filled','colorgroup',[1:6],'colors',hot(15),'labels',{'A0', 'A1', 'A2', 'A3', 'A4', 'A5'});
%     title('Age group targeted')
% catch
% end
% % 
% % %EffectiveCoverage
% % % subplot(2,3,2)
% % % vec=nan;    g=nan;
% % % for effind=1:2
% % %     indices=find(Scenario(:,2)==EffectiveCoverage(effind));
% % %     vec=[vec;Reductions(indices,:)];
% % %     g=[g;effind*ones(length(indices),1)];
% % % end
% % % %H=notBoxPlot(vec(2:end),g(2:end),'markMedian',true); MakeNotBoxPlotPretty( H )
% % % boxplot(vec(2:end),g(2:end),'Boxstyle','filled','colorgroup',[1:2],'colors',hot(10),'labels',{'50 x 90%','70 x 50%'});
% % % title('EffectiveCoverage')
% % 
% % 
% % 
% %Sterilizing
% subplot(2,3,2)
% vec=nan;    g=nan;
% for sterind=1:2
%     indices=find(Scenario(:,3)==Sterilizing(sterind));
%     vec=[vec;Reductions(indices,:)];
%     g=[g;sterind*ones(length(indices),1)];
% end
% %H=notBoxPlot(vec(2:end),g(2:end),'markMedian',true); MakeNotBoxPlotPretty( H )
% boxplot(vec(2:end),g(2:end),'Boxstyle','filled','colorgroup',[1:2],'colors',hot(10),'labels',{'No','Yes'}); 
% title('Sterilizing')
% % 
% % 
% %Susceptibility reduction
% subplot(2,3,3)
% vec=nan;    g=nan;
% for susind=1:5
%     indices=find(Scenario(:,4)==SusceptibilityReduction(susind));
%     vec=[vec;Reductions(indices,:)];
%     g=[g;susind*ones(length(indices),1)];
% end
% %H=notBoxPlot(vec(2:end),g(2:end),'markMedian',true); MakeNotBoxPlotPretty( H )
% boxplot(vec(2:end),g(2:end),'Boxstyle','filled','colorgroup',[1:5],'colors',hot(10),'labels',{'0%','30%','50%','70%','90%'}); 
% title('Susceptibility')
% 
% 
% %Infectiousness reduction
% subplot(2,3,4)
% vec=nan;    g=nan;
% for infind=1:5
%     indices=find(Scenario(:,5)==InfectiousnessReduction(infind));
%     vec=[vec;Reductions(indices,:)];
%     g=[g;infind*ones(length(indices),1)];
% end
% %H=notBoxPlot(vec(2:end),g(2:end),'markMedian',true); MakeNotBoxPlotPretty( H )
% boxplot(vec(2:end),g(2:end),'Boxstyle','filled','colorgroup',[1:5],'colors',hot(10),'labels',{'0%','30%','50%','70%','90%'});
% title('Infectiousness')
% 
% %Symptom reduction
% subplot(2,3,5)
% vec=nan;    g=nan;
% for symind=1:5
%     indices=find(Scenario(:,6)==SymptomReduction(symind));
%     vec=[vec;Reductions(indices,:)];
%     g=[g;symind*ones(length(indices),1)];
% end
% %H=notBoxPlot(vec(2:end),g(2:end),'markMedian',true); MakeNotBoxPlotPretty( H )
% boxplot(vec(2:end),g(2:end),'Boxstyle','filled','colorgroup',[1:5],'colors',hot(10),'labels',{'0%','30%','50%','70%','90%'});
% title('Symptoms')
% % 
% % % %Vaccine duration
% % % subplot(2,4,7)
% % % vec=nan;    g=nan;
% % % for durind=1:3
% % %     indices=find(Scenario(:,7)==VaccineDuration(durind));
% % %     vec=[vec;Reductions(indices,:)];
% % %     g=[g;durind*ones(length(indices),1)];
% % % end
% % % %H=notBoxPlot(vec(2:end),g(2:end),'markMedian',true); MakeNotBoxPlotPretty( H )
% % % boxplot(vec(2:end),g(2:end),'Boxstyle','filled','colorgroup',[1:3],'colors',hot(5),'labels',{'1','3','5'});
% % % title('Vaccine duration')

%% cases averted by age
figure('Color',[1 1 1])
for i=1:6
indices=find(Scenario(:,1)==i); subplot(2,3,i);
boxplot(Reduction_by_age(:,indices).','Boxstyle','filled','colorgroup',[1:7],'colors',hot(15),'labels',{'0-8', '8-18', '18-26', '26-37','37-50','50-70','70+'});
title(i)
end

end

