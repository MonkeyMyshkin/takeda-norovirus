%%% SCRIPT TO PLOT the susceptible population with and without clearing of
%%% infection

part =  median(PARTICLE);

Lmax=81;

%DEFINE PARAMETERS
Param=part(1:9);   %alpha, q, omega1,  nu, delta, epsilon, sigma, psi, gamma, chi
ReportingBaseline = [part(10) *[1 part(11)*ones(1,2) part(12) part(13) part(14)] part(15)];
k=part(17);
d=part(18);
quenching=part(19:20);
theta=part(21:end);

%contact and ICs
[ NewContactMatrix ] = ContactTwist( ContactAgesPerAgeGroup, T, k, d );
x0=MakeInitialConditions( Param,omega2,theta,mu , NewContactMatrix );

%SIMULATE WITHOUT VACCINATION
[ ~,SimulationResult ] =SimulateSeasons(Param(1:9),omega2 ,mu, theta, NewContactMatrix , x0);
[ ModelOutput ,ModelOutputAll ] =  ProcessCases( SimulationResult, ageGroupBreaks );
[ ModelOutput_asymp ] =  ProcessCases_asymp( SimulationResult, ageGroupBreaks );


%Vaccine simulation and parameters
EffectiveCoverage=[0.5*0.9 0.7*0.5]/365;
A0=[1;zeros(Lmax-1,1)];
A1=[0;1;zeros(Lmax-2,1)];
A2=[0;0;1;zeros(Lmax-3,1)];
A3=[zeros(50,1); ones(20,1);zeros(Lmax-70,1)];
A4=[zeros(70,1); ones(11,1)];
A5=zeros(Lmax,1); A5([66,71,76,81])=1;
A6=[zeros(50,1); ones(31,1)];

AgeGroupsTargeted=[A0, A1, A1 + A6, A1 + A5, A4, A5];

for agegrp = 1:6
    for kappa=0:1
        sigmaV = 0.5;
        phi=[EffectiveCoverage(1)*AgeGroupsTargeted(1:30,agegrp);EffectiveCoverage(2)*AgeGroupsTargeted(31:end,agegrp)];
        %kappa=0; %or 1
        tau=0.5;
        zeta=0.5;
        SigmaV=0.5;
        rho=1/(5*365);
        
        sigmaV=sigmaV*Param(7);   %proportion of infected vaccianted individuals who are symptomatic
        ParamVac=[rho, kappa,tau, zeta, sigmaV];
        
        %SIMULATE WITH VACCINATION
        [~,SimulationResultVac ] = SimulateSeasons_Vaccination( Param,ParamVac,omega2,mu ,phi.',theta, NewContactMatrix , x0);
        %[~,SimulationResultVac ] = SimulateLifetime_Vaccination( Param,ParamVac,mu ,phi,mean(theta), NewContactMatrix , x0);
        [ ModelOutputVac,Vacc ,ModelOutputVacAll] =  ProcessCases_Vac( SimulationResultVac, ageGroupBreaks );
        [ ModelOutputVac_asymp,Vacc] =  ProcessCases_Vac_asymp( SimulationResultVac, ageGroupBreaks );
        
        %SCALE UP TO POPULATION LEVEL
        PopModelOutput=ModelOutput*sum(PopulationSize);
        PopModelOutputAll=ModelOutputAll*sum(PopulationSize);
        PopModelOutputVacAll=ModelOutputVacAll*sum(PopulationSize);
        PopModelOutputVac=ModelOutputVac*sum(PopulationSize);
        PopModelOutput_asymp=ModelOutput_asymp*sum(PopulationSize);
        PopModelOutputVac_asymp=ModelOutputVac_asymp*sum(PopulationSize);
        
        load('GermanPopulation.mat')
        Vacc=Vacc.*sum(PopulationSize)/418 *52;
        
        %OPTIONAL FIGURE
        
        for index=1:7
            subplot(4,2,index)
            plot(PopModelOutput_asymp(:,index),'r')
            hold on
            plot(PopModelOutputVac(:,index),'g')
        end
        
        sus(kappa+1, agegrp) = mean(sum(SimulationResultVac(:,[Lmax+1:2*Lmax, 7*Lmax+1:8*Lmax] ).'));
    end
end
figure
figure('Color',[1 1 1])
bar(sus,'DisplayName','sus')

set(h,'FontSize',25, ...
    'xticklabel',{'Clearance','No clearance'},'XTick',[0,1])
ylabel('Mean susceptible proportion')