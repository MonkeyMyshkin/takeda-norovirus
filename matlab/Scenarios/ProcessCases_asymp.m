function [ ProbabilityOfInfection ] = ProcessCases_asymp( SimulationResult, AgeGroupBreaks )
%PROCESSCASES: takes cumulative case numbers for each week, finds the
%number of cases in each week and stratifies in age groups
Lmax=length(SimulationResult(1,:))/9;

C=SimulationResult(:,8*Lmax+1:9*Lmax);

Cases(1,:)=C(1,:);

Cases(2:length(SimulationResult(:,1)),:)= max(C(2:end,:)-C(1:end-1,:),0);

Cases=AgeStratify(Cases,AgeGroupBreaks);

%need to find the size of each age group to get the probability

for index=1:Lmax
    ageGroupSize(:,index)=sum(SimulationResult(:,[index,Lmax+index,2*Lmax+index,3*Lmax+index,4*Lmax+index,5*Lmax+index,6*Lmax+index]),2);
end

ageGroupSize=AgeStratify(ageGroupSize, AgeGroupBreaks);

ProbabilityOfInfection=bsxfun(@rdivide,Cases,sum(ageGroupSize,2)); %Probability an individual is age a, infected  in week i

end

