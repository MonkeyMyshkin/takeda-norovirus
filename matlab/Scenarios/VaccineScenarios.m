addpath(genpath('../takeda-norovirus'))
%Program to random sample vaccine scenarios and measure: cases averted,
%outpatient visits averted, hospitalisations and deaths averted per dose
%per year
Lmax=81;
PARTICLE=exp(PARTICLE);

%scenario space is 7 dimensional:
% Effective_Coverage, Age groups targeted, Vaccine duration, Symptom
% reduction, Infectiousness reduction, Susceptibility reduction, immunizing
EffectiveCoverage=[0.5*0.9 0.7*0.5]/365;

A0=[1;zeros(Lmax-1,1)];
A1=[0;1;zeros(Lmax-2,1)];
A2=[0;0;1;zeros(Lmax-3,1)];
A3=[zeros(50,1); ones(20,1);zeros(Lmax-70,1)];
A4=[zeros(70,1); ones(11,1)];
A5=zeros(Lmax,1); A5([66,71,76,81])=1;
A6=[zeros(50,1); ones(31,1)];  
AgeGroupsTargeted=[A0, A1, A1 + A6, A1 + A5, A4, A5];

VaccineDuration=1/(5*365);

SymptomReduction=[0 0.3 0.5 0.7 1]; %this scales the proportion symptomatic

InfectiousnessReduction=[0 0.3 0.5 0.7 1];

SusceptibilityReduction=[0 0.3 0.5 0.7 1];

Sterilizing=[1 0];

%proportion outpatient, hospitilized or dead
outpatient=[0.168*ones(1,5), 0.168*ones(1,10), 0.062*ones(1,10), 0.064*ones(1,20), 0.054*ones(1,20), 0.103*ones(1,16)];
hospitilized=[0.00428*ones(1,5), 0.00182*ones(1,13), 0.00228*ones(1,47), 0.01733*ones(1,16)];
fatalities=[6.25e-6*ones(1,5),4.66e-6*ones(1,60),4.35e-4*ones(1,16)];

%For a number of samples of scenario space, calculate reduction in cases
NoScenarios=10000;
NoSamples=1;

tic
parfor index=1:NoScenarios
    AgeGrp=randsample(6,1);   
    phi=[EffectiveCoverage(1)*AgeGroupsTargeted(1:30,AgeGrp);EffectiveCoverage(2)*AgeGroupsTargeted(31:end,AgeGrp)];
    kappa=randsample(Sterilizing,1);
    tau=randsample(SusceptibilityReduction,1);
    zeta=randsample(InfectiousnessReduction,1);
    SigmaV=randsample(SymptomReduction,1);
    rho=VaccineDuration;
    
    sampleIndices=randsample(length(PARTICLE(:,1)),NoSamples);
    
    for jndex=1:NoSamples
        
         [Red,Red_asymp,Vac]=ReductionInCases( PopulationSize,ageGroupBreaks, ContactAgesPerAgeGroup, ...
            T, median(PARTICLE),omega2, mu, phi.',rho, kappa, tau,zeta,SigmaV,0);
%         [Red,Red_asymp, Vac]=ReductionInCases( PopulationSize,ageGroupBreaks, ContactAgesPerAgeGroup, ...
%             T, (PARTICLE(sampleIndices(jndex),:)),omega2, mu, phi.',rho, kappa, tau,zeta,SigmaV,0 );
        
        Reduction(index,jndex)=sum(Red);
        
        Reduction_asymp(index,jndex)=sum(Red_asymp);
        Reduction_by_age(:, index,jndex)= Red_asymp;
        
        Vac=bsxfun(@times,Vac,[(1/0.5)*ones(1,30),(1/0.7)*ones(1,51)]);    %to account for vaccinated without effect
        
        Vaccinated(index,jndex)=sum(Vac(:));
        
        %outpatient, hospitilization and mortality \cite{Bartsch2012}
        OUTPATIENT(index,jndex)=sum(Red.*outpatient);
        HOSPITILIZED(index,jndex)=sum(Red.*hospitilized);
        FATALITIES(index,jndex)=sum(Red.*fatalities);
    end
    EffCov=nan;
    Scenario(index,:)=[AgeGrp, EffCov, kappa, tau, zeta, SigmaV, rho];
    
end
Reduction=Reduction(:); Reduction_asymp=Reduction_asymp(:); 

Vaccinated=Vaccinated(:);
OUTPATIENT=OUTPATIENT(:);   HOSPITILIZED=HOSPITILIZED(:);   FATALITIES=FATALITIES(:);
Scenario=repmat(Scenario,NoSamples,1);

time=toc
save('VACCINESCENARIOS_Takeda_Scenarios_PLUS_1011_MEDIAN_perdose_NewM')


%% remove null scenario
ind=sum(Scenario(:,[4 5 6])==[1 1 1],2)==3;

indices=find(ind==0);
Scenario=Scenario(indices,:);
Reduction=Reduction(indices);
Reduction_asymp=Reduction_asymp(indices);
Reduction_by_age=Reduction_by_age(:,indices);
Vaccinated=Vaccinated(indices);
OUTPATIENT=OUTPATIENT(indices);   HOSPITILIZED=HOSPITILIZED(indices);   FATALITIES=FATALITIES(indices);



%% outpatient

PlotVaccinationScenarios( Scenario,OUTPATIENT./Vaccinated, Reduction_by_age);
title('Outpatients');set(gcf,'color','w'); h=get(gcf,'children');
set(h,'FontSize',25)
ylabel('Cases averted per dose per year')
%% hospitilized
PlotVaccinationScenarios( Scenario,HOSPITILIZED./Vaccinated, Reduction_by_age);
title('Hospitalization');set(gcf,'color','w');h=get(gcf,'children');
set(h,'FontSize',25)
ylabel('Cases averted per dose per year')
%% dead

PlotVaccinationScenarios( Scenario,FATALITIES./Vaccinated, Reduction_by_age);
title('Deaths');set(gcf,'color','w');h=get(gcf,'children');
set(h,'FontSize',25)
ylabel('Cases averted per dose per year')
%% All
PlotVaccinationScenarios( Scenario,Reduction_asymp./Vaccinated, bsxfun(@rdivide,Reduction_by_age,Vaccinated.') );
h=get(gcf,'children');
set(h,'FontSize',18)
title('All infections');set(gcf,'color','w');
ylabel('Outcomes averted per dose per year')

%%
subplot(2,2,1)
PlotVaccinationScenarios( Scenario,Reduction_asymp./Vaccinated, Reduction_by_age);
h=get(gcf,'children');
set(h,'FontSize',25)
title('All infections');set(gcf,'color','w');
ylabel('Outcomes averted per dose per year')
subplot(2,2,2)
PlotVaccinationScenarios( Scenario,OUTPATIENT./Vaccinated, Reduction_by_age);
title('Outpatients');set(gcf,'color','w'); h=get(gcf,'children');
set(h,'FontSize',25)

subplot(2,2,3)
PlotVaccinationScenarios( Scenario,HOSPITILIZED./Vaccinated, Reduction_by_age);
title('Hospitalization');set(gcf,'color','w');h=get(gcf,'children');
set(h,'FontSize',25)

subplot(2,2,4)
PlotVaccinationScenarios( Scenario,FATALITIES./Vaccinated, Reduction_by_age);
title('Deaths');set(gcf,'color','w');h=get(gcf,'children');
set(h,'FontSize',25)

%%
subplot(2,2,1)
PlotVaccinationScenarios( Scenario,Reduction_asymp, Reduction_by_age);
h=get(gcf,'children');
set(h,'FontSize',25)
title('All infections');set(gcf,'color','w');
ylabel('Outcomes averted per year')
subplot(2,2,2)
PlotVaccinationScenarios( Scenario,OUTPATIENT, Reduction_by_age);
title('Outpatients');set(gcf,'color','w'); h=get(gcf,'children');
set(h,'FontSize',25)

subplot(2,2,3)
PlotVaccinationScenarios( Scenario,HOSPITILIZED, Reduction_by_age);
title('Hospitalization');set(gcf,'color','w');h=get(gcf,'children');
set(h,'FontSize',25)

subplot(2,2,4)
PlotVaccinationScenarios( Scenario,FATALITIES, Reduction_by_age);
title('Deaths');set(gcf,'color','w');h=get(gcf,'children');
set(h,'FontSize',25)

%% TABLE OF RESULTS
% agegrp=[{'A0', 'A1', 'A2', 'A3', 'A4', 'A5'}]
for i=1:6
indices=find(Scenario(:,1)==i);

TableVac(i)=median(Vaccinated(indices))/1e6;
TableCase(:,i)=quantile(Reduction_asymp(indices)./Vaccinated(indices),[0.5,0.025,0.97]).' *1e6;
TableOut(:,i)=quantile(OUTPATIENT(indices)./Vaccinated(indices),[0.5,0.025,0.97]).'*1e6;
TableHos(:,i)=quantile(HOSPITILIZED(indices)./Vaccinated(indices),[0.5,0.025,0.97]).'*1e6;
TableDea(:,i)=quantile(FATALITIES(indices)./Vaccinated(indices),[0.5,0.025,0.97]).'*1e6;

if i>1
t=[t;table(TableVac(i)*ones(3,1), TableCase(:,i), TableOut(:,i), TableHos(:,i), TableDea(:,i),'VariableNames',...
    {'Doses', 'Cases','Outpatients', 'Hospitilizations','Deaths'})];
else
  t=table(TableVac(i)*ones(3,1), TableCase(:,i), TableOut(:,i), TableHos(:,i), TableDea(:,i),'VariableNames',...
    {'Doses', 'Cases','Outpatients', 'Hospitilizations','Deaths'});
end

end


% 
 writetable(t,'FinalFigs/DosesCases.xls');




%% just looking at clearance
vec=[3,4,5,6];
for i= 1%:4
    %subplot(2,2,i)
ind10=find(sum(Scenario(:,[1,vec(i)])==[1,min(Scenario(:,vec(i)))],2)==2 ); %strategy 1, clearance 0
ind11=find(sum(Scenario(:,[1,vec(i)])==[1,max(Scenario(:,vec(i)))],2)==2 );
ind50=find(sum(Scenario(:,[1,vec(i)])==[5,min(Scenario(:,vec(i)))],2)==2 ); %strategy 6, clearance 0
ind51=find(sum(Scenario(:,[1,vec(i)])==[5,max(Scenario(:,vec(i)))],2)==2 );

maxind=min([length(ind10),length(ind11),length(ind50),length(ind51)]);

boxplot([casesAvertPerDose(ind10(1:maxind)),casesAvertPerDose(ind11(1:maxind)),casesAvertPerDose(ind50(1:maxind)),...
    casesAvertPerDose(ind51(1:maxind))],'Boxstyle','filled','colorgroup',[1:4],'colors',hot(8),'labels',{'A0 with clearance','A0 without clearance','A4 with clearance','A4 without clearance'})

end
h=get(gcf,'children');
set(h,'FontSize',16)
set(gcf,'color','w');

%% Tornado plots SIMPLE
figure
% we shall do 6 situations, one for each strategy
% baseline is clearance=0
%             sus=0.5
             %inf=0.5
             %symp=0.5
             
for i=1:6
    subplot(2,3,i)
   indices=find(Scenario(:,1)==i);
   
   Scen=Scenario(indices,:);
Red=Reduction(indices);
Red_asymp=Reduction_asymp(indices);
Red_by_age=Reduction_by_age(:,indices);
Vacc=Vaccinated(indices);
OUT=OUTPATIENT(indices);   HOS=HOSPITILIZED(indices);   FAT=FATALITIES(indices);

%baseline infections averted
ind=sum(Scen(:,3:6)==[0 0.5 0.5 0.5],2)==3;
ind=find(ind==1);
baseline=Red_asymp(ind(1))./Vacc(ind(1));

%changing clearance
ind=sum(Scen(:,3:6)==[1 0.5 0.5 0.5],2)==3;
ind=find(ind==1);
clearance=Red_asymp(ind(1))./Vacc(ind(1));
clearance_change = (baseline - clearance)/baseline;

%changing sus
ind=sum(Scen(:,3:6)==[0 1 0.5 0.5],2)==3;
ind=find(ind==1);
sus=Red_asymp(ind(1))./Vacc(ind(1));
sus_change = (baseline - sus)/baseline;

%changing inf
ind=sum(Scen(:,3:6)==[0 0.5 1 0.5],2)==3;
ind=find(ind==1);
inf=Red_asymp(ind(1))./Vacc(ind(1));
inf_change = (baseline - inf)/baseline;

%changing sus
ind=sum(Scen(:,3:6)==[0 0.5 0.5 1],2)==3;
ind=find(ind==1);
symp=Red_asymp(ind(1))./Vacc(ind(1));
symp_change = (baseline - symp)/baseline;

%putting it together
changes= [clearance_change sus_change inf_change symp_change].';

h = barh(changes);
hold on
barh(-changes,'r')
bh = get(h,'BaseLine');
set(bh,'BaseValue',0);
title(i)

end

%% Tornado plots 
figure

%baseline is clearance=0.5
%             sus=0.5
             %inf=0.5
             %symp=0.5
kappa=0.5;
tau=0.5;
zeta=0.5;
SigmaV=0.5;
rho=VaccineDuration;

for AgeGrp=1:6
    subplot(2,3,AgeGrp)
    
    phi=[EffectiveCoverage(1)*AgeGroupsTargeted(1:30,AgeGrp);EffectiveCoverage(2)*AgeGroupsTargeted(31:end,AgeGrp)];
    
    %baseline
    [Red,Red_asymp,Vac]=ReductionInCases( PopulationSize,ageGroupBreaks, ContactAgesPerAgeGroup, ...
        T, median(PARTICLE),omega2, mu, phi.',rho, kappa, tau,zeta,SigmaV,0);
    
    Vac=(bsxfun(@times,Vac,[(1/0.5)*ones(1,30),(1/0.7)*ones(1,51)]));    %to account for vaccinated without effect
    Red=sum(Red);
    Red_asymp=sum(Red_asymp);
    
    baseline=Red_asymp/sum(Vac(:));
    
    %change clearance
    [Red,Red_asymp,~]=ReductionInCases( PopulationSize,ageGroupBreaks, ContactAgesPerAgeGroup, ...
        T, median(PARTICLE),omega2, mu, phi.',rho, kappa *1.1, tau,zeta,SigmaV,0);
    
    
    Red=sum(Red);
    Red_asymp=sum(Red_asymp);
    clearHigh=Red_asymp/sum(Vac(:));
    [Red,Red_asymp,~]=ReductionInCases( PopulationSize,ageGroupBreaks, ContactAgesPerAgeGroup, ...
        T, median(PARTICLE),omega2, mu, phi.',rho, kappa *0.9, tau,zeta,SigmaV,0);
    
   
    Red=sum(Red);
    Red_asymp=sum(Red_asymp);
    clearLow=Red_asymp/sum(Vac(:));
    
    %change sus
    [Red,Red_asymp,~]=ReductionInCases( PopulationSize,ageGroupBreaks, ContactAgesPerAgeGroup, ...
        T, median(PARTICLE),omega2, mu, phi.',rho, kappa , tau*1.1,zeta,SigmaV,0);
    
    
    Red=sum(Red);
    Red_asymp=sum(Red_asymp);
    susHigh=Red_asymp/sum(Vac(:));
    [Red,Red_asymp,~]=ReductionInCases( PopulationSize,ageGroupBreaks, ContactAgesPerAgeGroup, ...
        T, median(PARTICLE),omega2, mu, phi.',rho, kappa , tau*0.9,zeta,SigmaV,0);
    
    
    Red=sum(Red);
    Red_asymp=sum(Red_asymp);
    susLow=Red_asymp/sum(Vac(:));
    
    %change inf
    [Red,Red_asymp,~]=ReductionInCases( PopulationSize,ageGroupBreaks, ContactAgesPerAgeGroup, ...
        T, median(PARTICLE),omega2, mu, phi.',rho, kappa , tau,zeta*1.1,SigmaV,0);
    
    
    Red=sum(Red);
    Red_asymp=sum(Red_asymp);
    infHigh=Red_asymp/sum(Vac(:));
    [Red,Red_asymp,~]=ReductionInCases( PopulationSize,ageGroupBreaks, ContactAgesPerAgeGroup, ...
        T, median(PARTICLE),omega2, mu, phi.',rho, kappa , tau,zeta*0.9,SigmaV,0);
    
   
    Red=sum(Red);
    Red_asymp=sum(Red_asymp);
    infLow=Red_asymp/sum(Vac(:));
    
    %change symp
    [Red,Red_asymp,~]=ReductionInCases( PopulationSize,ageGroupBreaks, ContactAgesPerAgeGroup, ...
        T, median(PARTICLE),omega2, mu, phi.',rho, kappa , tau,zeta,SigmaV*1.1,0);
    
    
    Red=sum(Red);
    Red_asymp=sum(Red_asymp);
    sympHigh=Red_asymp/sum(Vac(:));
    [Red,Red_asymp,~]=ReductionInCases( PopulationSize,ageGroupBreaks, ContactAgesPerAgeGroup, ...
        T, median(PARTICLE),omega2, mu, phi.',rho, kappa , tau,zeta,SigmaV*0.9,0);
    
    
    Red=sum(Red);
    Red_asymp=sum(Red_asymp);
    sympLow=Red_asymp/sum(Vac(:));
    
    
    %putting it together
    changesHigh=(baseline - [clearHigh, susHigh, infHigh, sympHigh])/baseline;
    changesLow=(baseline - [clearLow, susLow, infLow, sympLow])/baseline;
    
    h = barh(changesHigh);
    hold on
    barh(changesLow,'r')
    bh = get(h,'BaseLine');
    set(bh,'BaseValue',0);
    title(AgeGrp)
end
  
%% finding percenatage change

for AgeGrp=1:6

    
    indices=find(Scenario(:,1)==AgeGrp);
% load('no_infections.mat')
% disp('Maximum change in infections is')
inf=quantile(Reduction_asymp(indices)/sum(no_infections),[0.5,0.025,0.975]);

% load('no_cases.mat')
% disp('Maximum change in cases is')
% quantile(Reduction(indices)/sum(no_cases),[0.5,0.025,0.975]);     
   
if AgeGrp>1
t=[t;table(AgeGrp,inf,'VariableNames',{'Strategy','Infections'})];
else
  t=table(AgeGrp,inf,'VariableNames',{'Strategy','Infections'});
end

end    
        
        
  writetable(t,'FinalFigs/percentage.xls');       
        