MATLAB code introduction
========================

This code was produced by KAMG and produces the figures shown in
Gaythorpe, Trotter and Conlan (2018).

Description
===========

Catalytic
---------

To be removed

NoroModel
---------

Original model description andfunctions for simulating the model and
calculating the likelihood etc. This also contains inputs such as the
start conditions and seasonal offset, omega2.

NoroModelLog
------------

This contains the final form of the model calibration SMC script where
the parameters to be estimated are log-transformed to improve estimation
efficiency.

SMC
---

This contains older versions of the SMC procedure as well as
"PARTICLE\_OUTPUT.mat" which is the output used to generate the results
in the accompanying paper. There are also scripts to produce plots of
the fit and inference characteristics.

Scenarios
---------

Vaccination scenarios and output. This contains scripts to calculate
impact and plot vaccination characteristics.
