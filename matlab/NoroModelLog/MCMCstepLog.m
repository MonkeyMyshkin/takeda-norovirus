function [ AcceptReject, AccProp, LogLik ] = MCMCstepLog(ParamVector,omega2,mu,theta, NewContactMatrix ,x0, AccCurrent ,  ageGroupBreaks,  GermanCaseNotification,PopulationSize,GermanPopulation,Dispersion,k, d,quenching)
%takes proposed values and assesses them
Lmax=length(mu);    %number of age years

%takes parameters wihtout loog transform
            
%Simulate epidemic
[ ~,SimulationResult ] =SimulateSeasons(ParamVector(1:9),omega2,mu ,theta, NewContactMatrix , x0);

%stratify data
[ StratifiedCases ] = AgeStratify( GermanCaseNotification, ageGroupBreaks );


%stratify like data
[ ModelOutput ,~] =  ProcessCases( SimulationResult, ageGroupBreaks );        %stratify infected individuals

ModelOutput=ModelOutput.*sum(PopulationSize);

%ReportingParam is magnitude of reporting factor
[ ReportingBaseline ] = [ParamVector(10)*([1 ParamVector(11)*ones(1,2) ParamVector(12) ParamVector(13) ParamVector(14)]) ParamVector(15)];  
                        %children reporting are baseline

[ ReportedInfectionNumber] = CappedReporting( ModelOutput, ReportingBaseline, quenching );

%LIKELIHOOD
%%%all are structured in age groups, including population sizes
[ LogLik ] = NBLikelihood( StratifiedCases, ReportedInfectionNumber, Dispersion);



PriorProbs=PriorsLog( log(ParamVector(1:9)), log(ParamVector(10:15)),log(quenching),GermanPopulation,NewContactMatrix,mu,ageGroupBreaks);

%acceptance
AccProp=LogLik+PriorProbs;


[ AcceptReject ] = ACCEPTSynth( AccProp, AccCurrent);

end

