function [SimulationResult ] = PlotMeanFitSynth( Param,omega2, x0, contactMatrix , ageGroupBreaks, GermanCaseNotification,mu, quench, thetapar, Lmax,PopulationSize,Dispersion,justmean)
%UNTITLED8 Summary of this function goes here

colours=['b','r','y','g','m','c','b'];


%Simulate epidemic
[ ~,SimulationResult ] =SimulateSeasons(Param(1:9),omega2,mu ,thetapar, contactMatrix , x0);

%stratify data
[ StratifiedCases ] = AgeStratify( GermanCaseNotification, ageGroupBreaks );
%SIMULATED DATA

%stratify like data
[ ModelOutput, ~] =  ProcessCases( SimulationResult, ageGroupBreaks );        %stratify infected individuals

ModelOutput=ModelOutput.*sum(PopulationSize);

%ReportingParam is magnitude of reporting factor
[ ReportingBaseline ] = [Param(10)*[1 Param(11)*ones(1,2) Param(12) Param(13) Param(14)] Param(15)];

[ ReportedInfectionNumber] = CappedReporting( ModelOutput, ReportingBaseline, quench );

[ LogLik ] = NBLikelihood( StratifiedCases, ReportedInfectionNumber, Dispersion);

noSamples=1;
%generate negative binomial noise
%with overdispersion %define reported infection number as mean
P=repmat(bsxfun(@rdivide,bsxfun(@times,Dispersion,ReportedInfectionNumber),bsxfun(@plus,1,bsxfun(@times,Dispersion,ReportedInfectionNumber)) ),[1,1,noSamples]); %probability of fail

R=1/Dispersion; %number of successes
NoisyModel=nbinrnd(R,1-P);

%plot cases
for index=1:length(ageGroupBreaks)+1
    subplot(4,2,index)
    hold on
    col=colours(index);
    colsym=[colours(index)];
    if justmean==1
        ModelOutput=ReportedInfectionNumber(:,index)/PopulationSize(index);
        p=plot(ModelOutput,'LineWidth',1,'color','g');
        p.Color(4)=0.5;
    else
        for jndex=1:noSamples
            p=plot(NoisyModel(:,index,jndex).'/PopulationSize(index),'color',[57,197,218]/255);
            p.Color(4)=0.2;
        end
    end
end

for index=1:length(ageGroupBreaks)+1
    subplot(4,2,index)
    hold on
    plot(StratifiedCases(:,index)/PopulationSize(index),'LineWidth',2,'color','k');
end


end

