function [NoisyModel] = PlotChainIndex( index, x, z,justmean,omega2)
%function to plot output for certain chain inindex

%define parameters at this point
ParamFinal=(x(index,1:15));
dispFinal=(x(index,16));

%susceptibility profile
theta=z(index,:);

%contact parameters
kFinal=(x(index,17));
dFinal=(x(index,18));

quench=(x(index,19:20));

%prelim studd
load('..\..\..\takeda-norovirus\data\GermanCaseNotification.mat')
load('..\..\..\takeda-norovirus\data\GermanPopulation.mat')
load('..\..\..\takeda-norovirus\data\GermanContactData.mat')
load('..\..\..\takeda-norovirus\data\GermanParticipantData.mat')
listOfIDs=GermanParticipantData(:,1);
%death function, population distribution
%maximum age group in case notifications is 80. Therefore our max age will
%be 80, indexed as 81.
MaxAge=80;
Lmax=MaxAge+1;
noSeasons=9;
%MU
mu=MakeMu(Lmax);    %daily death rate
%AGEGROUPS
ageGroupBreaks=[8 18 26 37 50 70];
noAgeGroups=length(ageGroupBreaks)+1;
PopulationSize=AgeStratify(GermanPopulation.',ageGroupBreaks);
%omega2=[0.29345  0.37976  0.56963  0.60415  0.82855  1.0724  1.0357];

[ ~,ContactAgesPerAgeGroup,T] = MakeContactMatrix( ageGroupBreaks,  GermanContactData , GermanParticipantData );
[ NewContactMatrix ] = ContactTwist( ContactAgesPerAgeGroup, T, kFinal, dFinal );

x0=MakeInitialConditions( ParamFinal,omega2,theta,mu , NewContactMatrix );
NoisyModel=PlotMeanFitSynth( ParamFinal,omega2, x0, NewContactMatrix , ageGroupBreaks, GermanCaseNotification,mu, quench, theta,Lmax,PopulationSize,dispFinal,justmean);

end

