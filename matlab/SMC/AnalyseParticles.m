%script that will take particle output and produce figures
addpath('..\..\..\takeda-norovirus\matlab\NoroModel')
%% noIm
x=exp(PARTICLE(:,1:20));
y=LL;
z=exp(PARTICLE(:,21:end));


%plot maximum likelihood value
figure
[maximum,maxlike]=max(y);
disp(maximum)
% hold on
x0=PlotChainIndex( maxlike, x, z,1,(omega2));

[ NewContactMatrix ] = ContactTwist( ContactAgesPerAgeGroup, T, x(maxlike,17), x(maxlike,18));
R0=MakeR0( x(maxlike,1:9) ,GermanPopulation,NewContactMatrix, Lmax, mu, ageGroupBreaks)

%% Parameter correlations
EstIndex=[3:4,9:12,14:15,19:20];
NAMES={'\omega_1','\nu','\gamma','Rep0-8','Rep8-26','Rep26-50','Rep50-70','Rep70+',...
     '\tau_1','\tau_2'};
figure('Color',[1 1 1])
[S,AX,BigAx,H,HAx] =plotmatrix(PARTICLE(:,EstIndex),'k.');  h=get(gcf,'children'); set(h,'FontSize',10)
Correlations=corrcoef(PARTICLE(:,EstIndex));
colors=hot(11);
Correlations=abs(round(Correlations,1));
colormapvec=linspace(0,1,11);

for i =1:length(EstIndex)
    for j =1:length(EstIndex)
        colorind=find((colormapvec<Correlations(i,j)+0.05 & colormapvec>Correlations(i,j)-0.05));
        S(i,j).Color=colors(12-colorind,:);
        H(1,j).FaceColor=[0 0 0];
    end
    
end
h=get(gcf,'children');
set(h,'FontSize',12)
title(BigAx, 'Parameter correlations')
xlabel(BigAx,'Parameter value'); ylabel(BigAx,'Density')

savefig('FinalFigs/Correlations.fig')
set(gcf, 'Position', get(0, 'Screensize'));
saveas(gcf,'FinalFigs/Images/Correlations.png')

%% write parameter values
M(1,:)=median(PARTICLE(:,EstIndex));
M(2,:)=quantile(PARTICLE(:,EstIndex),0.025);
M(3,:)=quantile(PARTICLE(:,EstIndex),0.975);
csvwrite('FinalFigs/ParameterValues.csv',exp(M))
%%
figure('Color',[1 1 1])
plot(time/3600)
%%
figure('Color',[1 1 1])
plot(max(Like.'));hold on
plot(mean(Like.'));
plot(min(Like.'))


savefig('FinalFigs/Like.fig')
saveas(gcf,'FinalFigs/Images/Like.png')
%%
%all
figure('Color',[1 1 1])
AgeGroups={'0-8','8-18','18-26','26-37','37-50','50-70','70+'};
%fit using samples from the chain
k=20;
sampleIndices=randsample(length(y),k);    %collect a vector of indices for 100 samples of chain

for i=1:k
    hold on
    modelOutput(:,:,i)=PlotChainIndex( sampleIndices(i), x, z,0,(omega2));
    drawnow
    h=get(gcf,'children');
 
end

load('GermanCaseNotification.mat')
StratifiedCases= AgeStratify( GermanCaseNotification, ageGroupBreaks );
medmod=median(modelOutput,3);
for index=1:7
    subplot(4,2,index) ; hold on
    plot(medmod(:,index).'/PopulationSize(index),'k-');
    title(AgeGroups(index))
end

set(h,'xlim',[0,418],'FontSize',25, ...
    'xticklabel',{'2008','2009','2010','2011','2012','2013','2014','2015','2016'},'XTick',[33,85,137,189,241,293,345,397])
ylabel('Incidence')

savefig('FinalFigs/Fit.fig')
set(gcf, 'Position', get(0, 'Screensize'));
saveas(gcf,'FinalFigs/Images/Fit.png')
%% susceptibility

%% reporting
figure('Color',[1 1 1]);
boxplot(exp([PARTICLE(:,10) + [ones(length(PARTICLE(:,1)),1), PARTICLE(:,11),PARTICLE(:,12),PARTICLE(:,14)],PARTICLE(:,15)]),...
    'boxstyle','filled','colorgroup',1:5,'colors',hot(10),'Labels',{'0-8','8-26','26-50','50-70','70+'});
h = findobj(gca, 'type', 'text');
set(gca,'FontSize',20);
ylabel('Reported proportion')

ReportingParam=exp(PARTICLE(:,10:15));
overall_rep=mean([ReportingParam(:,1).*[ones(length(PARTICLE(:,1)),9) ReportingParam(:,2).*ones(length(PARTICLE(:,1)),18) ...
    ReportingParam(:,3).*ones(length(PARTICLE(:,1)),11) ReportingParam(:,4).*ones(length(PARTICLE(:,1)),13) ReportingParam(:,5).*ones(length(PARTICLE(:,1)),20)] ReportingParam(:,6).*ones(length(PARTICLE(:,1)),10)]);

averageNoReported=1./overall_rep;
quantile(averageNoReported,[0.5,0.025,0.975])

savefig('FinalFigs/Reporting.fig')
set(gcf, 'Position', get(0, 'Screensize'));
saveas(gcf,'FinalFigs/Images/Reporting.png')
%%
for i=1:IterIndex
    ess(i)=ESS(WeightHistory(i,:));
end
figure('Color',[1 1 1]);plot(ess);title('ESS')

%%
%posterior
EstIndex=[3:4,9:12,14:15,19:20];    %EstIndex=[2:5,9:11,13:17,19:20];
% NAMES={'q','\omega_1','\nu','\delta','\gamma','Rep0-8','Rep8-26','Rep26-50','Rep50-70','Rep70+',...
%     'Dispersion','k','\tau_1','\tau_2'};
NAMES={'\omega_1','\nu','\gamma','Rep0-8','Rep8-26','Rep26-50','Rep50-70','Rep70+',...
     '\tau_1','\tau_2'};
figure('Color',[1 1 1])
hold on
for j=1:length(EstIndex)
    subplot(3,4,j)
    hold on
    pd=fitdist(PARTICLE(:,EstIndex(j)),'kernel','Bandwidth',0.05);
    x_values=linspace(max(min(PARTICLE(:,EstIndex(j))),-14) ,max(PARTICLE(:,EstIndex(j))) );
    y=pdf(pd,x_values);
    plot(x_values,y,'LineWidth',2,'color','r')
    title(NAMES(j))
    if j==5
        ylabel('Density')
    end
end


h=get(gcf,'children');
savefig('FinalFigs/Posterior.fig')
set(gcf, 'Position', get(0, 'Screensize'));
saveas(gcf,'FinalFigs/Images/Posterior.png')
%% R0 noIM
R0Ave=500;%length(x(:,1));%

R0Indices=1:100;%length(x(:,1));%
for i=1:R0Ave%length(x(:,1))
    R0Indices(i)=i;
    [ NewContactMatrix ] = ContactTwist( ContactAgesPerAgeGroup, T, x(R0Indices(i),17), x(R0Indices(i),18));
    R0(i)=MakeR0( x(R0Indices(i),1:9) ,GermanPopulation,NewContactMatrix, Lmax, mu, ageGroupBreaks);
end
subplot(4,4,13:16)
pd=fitdist(R0.','kernel');
x_values=linspace(min(R0), max(R0));
y=pdf(pd,x_values);
plot(x_values,y,'LineWidth',2,'color','r')
title('R_0')
%Prior
hold on
pd=makedist('normal','mu',15,'sigma',1);
x_values=linspace(min(R0),max(R0));
y=pdf(pd,x_values);
plot(x_values,y,'LineWidth',2,'color','k')

%%
%smoke plot
EstIndex=[2:5,9:12,14:17,19:20];
NAMES={'q','\omega_1','\nu','\delta','\gamma','Rep0-8','Rep8-26','Rep26-50','Rep50-70','Rep70+',...
    'Dispersion','k','\tau_1','\tau_2'};
figure('Color',[1 1 1])
for j=1:length(EstIndex)
    subplot(4,4,j)
    for i=1:IterIndex
        hold on; scatter(i*ones(1,NumberParticles),(PARTICLEhistory(:,EstIndex(j),i)),'filled','k');
        alpha 0.02
        
        %pause
    end
    title(NAMES(j))
end
savefig('FinalFigs/Smoke.fig')
set(gcf, 'Position', get(0, 'Screensize'));
saveas(gcf,'FinalFigs/Images/Smoke.png')