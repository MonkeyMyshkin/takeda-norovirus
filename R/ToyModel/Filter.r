source('ToyModel.R')
require('parallel')
require('Matrix')
require('parallel')
require('mnormt')
require('IMIFA')
require('matrixStats')
require('MCMCpack')

# Check posterior using MCMC

max_lik <- optim(par=(c(1,1)),fn=NBposterior,method='BFGS',control=list(fnscale=-1))

mcmc.fit<-MCMCmetrop1R(NBposterior, theta.init=max_lik$par, thin=1, mcmc=10000, burnin=0, tune=c(1), verbose=500, logfun=TRUE)


# Particle Filter
# specifics
noParam 		= 	2		# number of parameters to be estimated
noParticles		=	1000	# number of particles
noGenerations	= 	100 	# number of generations to filter
ncpus 			= 	24		# number of threads/cpus
# Initial covariance matrix for multivariate normal proposal distribution
init_proposal=(0.2^2)*diag(rep(1,noParam))/noParam

Generations <- list()

Generations[[1]] <- first_round(noParticles)
plot_posterior(Generations,1)
Generations[[2]] <- filter_particles(Generations[[1]],init_proposal)
plot_posterior(Generations,2)
Generations[[3]] <- filter_particles(Generations[[2]],init_proposal)
plot_posterior(Generations,3)

for(i in c(4:noGenerations))
{
Generations[[i]] <- filter_particles(Generations[[i-1]],init_proposal)
plot_posterior(Generations,i)
}







