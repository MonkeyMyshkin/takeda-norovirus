 # Generate toy data set, sample from two normal distributions
 obs_dat = cbind(rnorm(10000,mean=10+5,sd=2.0),rnorm(10000,mean=5*10,sd=2.0))
 
NBposterior <- function(x)
{

 lik = c(dnorm(obs_dat[,1],mean=(x[1]),sd=2,log=TRUE),dnorm(obs_dat[,2],mean=(x[2]),sd=2,log=TRUE))
 lik = sum(lik) + prior_prob(x)
 if(is.nan(lik) | is.na(lik)){return(-Inf)}else{return(lik)}
  
}

sample_prior <- function(n=1)
{
 prob = matrix(NA,n,2)
 prob[,1] = rexp(n,1)
 prob[,2] = rexp(n,1)
 
 return((prob)) 
}

prior_prob <- function(params)
{

prob = numeric(2)
 
prob[1] = dexp(params[1],1,log=T)
prob[2] = dexp(params[2],1,log=T)

return(sum(prob)) 
}

ESS <- function(weight)
{
#ESS calculates effective sample size based from (log)-weights
weight = exp(weight-max(weight))


# INPUTS
# weight=vector of particle weights 

# OUTPUTS
# ESS=effective samplesize

# Do not assume weights are normalised, so do so here
weight = weight / sum(weight)
return(1/sum(weight^2))
}

resample <- function(particle_type)
{

  particle_par 		= particle_type[[1]]
  particle_post		= particle_type[[2]]
  particle_weights	= particle_type[[3]]

  # Sample using (log) weights from previous round

  # Set weights to 1 if ESS falls below threshold
 
  #samp_ind = replicate(noParticles,gumbel_max(probs=t(particle_weights)))

  #samp_ind = #sample(1:noParticles,replace=TRUE,prob=exp(particle_weights-max(particle_weights)))
  
  samp_ind = resample_systematic(particle_weights)
  
  particle_par = particle_par[samp_ind]
  particle_post = particle_post[samp_ind]
  
  particle_weights = particle_weights[samp_ind]
     
return(list(param=unname(particle_par),post=unname(particle_post),weight= unname(particle_weights)))

}

filter_particles <- function(particle_type, proposal_cov)
{

 proposal_cov = as.matrix(nearPD(cov(2.0*do.call(rbind,particle_type[[1]])))$mat)

  particle_par	 		= particle_type[[1]]
  particle_post 		= particle_type[[2]]
  particle_weight 		= particle_type[[3]]


  new_particles = particle_type

  if(ESS(particle_weight) < noParticles/2)
  {
  print(paste('ESS = ',ESS(particle_type[[3]]), ' Resampling'))
  
  new_particles <- resample(new_particles)
  new_particles[[3]] = new_particles[[2]]
  }else{new_particles = particle_type}

 proposed_particles <- lapply(new_particles[[1]],function(x){
 val<-rmnorm(1,mean=x,varcov=proposal_cov);
 return(val)})

 proposed_posterior <- unlist(mclapply(proposed_particles,function(x)
 {
   # Test proposed values are in range 
   # two conditions- positive values and within prior ranges 
   pp = try(prior_prob(x),silent=TRUE)
   if(typeof(pp)=='character'){return(-Inf)
   }else{    
    if(min(exp(x)) < 0 | !is.finite(sum(pp)))  
    {return(-Inf)}else
    {return(NBposterior(x))}
    }
  },mc.cores=ncpus,mc.preschedule = FALSE))
  
  accept_reject <- sapply(1:noParticles,function(x){
  
   acc.prop = proposed_posterior[x]
   
   if(is.finite(acc.prop))
   {
   
   acc <- acc.prop - new_particles[[2]][x]
   acc <- exp(acc)
   u <- runif(1,0,1)
   if( u < acc)
   {
   return(TRUE)
   }else{return(FALSE)}
   
   }else{return(FALSE)}

  })
  
  # Only keep accepted particles, replace rejects with (resampled) particle from previous round
  proposed_particles[which(!accept_reject)] = new_particles[[1]][which(!accept_reject)]
  proposed_posterior[which(!accept_reject)] = new_particles[[2]][which(!accept_reject)]

  print(paste('Acceptance rate: ',mean(accept_reject)))
  
   TransitionKernel = unlist(mclapply(1:noParticles,function(x)
  { (logSumExp(sapply(1:noParticles,function(y){new_particles[[3]]+dmnorm(proposed_particles[[x]],mean=particle_par[[y]],varcov=proposal_cov,log=TRUE) + proposed_posterior[x] - particle_post[y]})))},mc.cores=ncpus))
  
 new_weights = proposed_posterior - TransitionKernel
   
return(list(param=proposed_particles,post=proposed_posterior,weight=new_weights))

}

first_round <- function(noParticles)
{
init_pop <- lapply(1:noParticles,function(x){sample_prior()})
out_list<-mclapply(init_pop,NBposterior)
particle_post <- unlist(out_list)

while(prod(is.finite(particle_post))==0)
{

for(i in which(!is.finite(particle_post)))
{
init_pop[[i]] = sample_prior()
particle_post[i] = NBposterior(init_pop[[i]])
}

}

prior_proposal<-unlist(mclapply(init_pop,function(x){sum(prior_prob(x))},mc.cores=ncpus))

# First round, weight particles by likelihood (density)
# Sampling from prior
weight = (particle_post)


return(list(param=init_pop,post=particle_post,weight=weight))

}


# The systematic resampling algorithm
# Coded by Maarten Speekenbrink, from 
# https://doi.org/10.1016/j.jmp.2016.05.006
# Adapted to work with log weights
resample_systematic <- function(weights) {
   weights = exp(weights-max(weights))
  # input: weights is a vector of length N with (unnormalized) importance weights
  # output: a vector of length N with indices of the replicated particles
  N <- length(weights)
  weights <- weights/sum(weights)# normalize weights
  csum <- cumsum(weights)
  u1 <- runif(1,min=0,max=1/N) # draw a single uniform number
  u <- c(0,seq(1/N,(N-1)/N,length=N-1)) + u1
  idx <- vector("integer",length=length(weights))
  j <- 1
  for(i in 1:N) {
    while (u[i] > csum[j]) {
      j <- j + 1
    }
    idx[i] <- j
  }
  return(idx)
}


plot_posterior <- function(Generations,i)
{
new_particles = Generations[[i]]
new_particles2 = resample(new_particles)
  
z<-do.call(rbind,new_particles[[1]])
z2<-do.call(rbind,new_particles2[[1]])
par(mfrow=c(3,2))
target <- (c(15,50))

for(j in 1:2)
{
truehist((z2[,j]),col='black')
abline(v=(target[j]),lwd=4,col='red')
}

plot(z[,1],(Generations[[i]][[2]]),pch=19)
plot(z[,2],(Generations[[i]][[2]]),pch=19)

plot(z[,1],(Generations[[i]][[3]]),pch=19)
plot(z[,2],(Generations[[i]][[3]]),pch=19)

}






